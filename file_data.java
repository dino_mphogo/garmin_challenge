import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
class file_data{
	final String file_1_name = "sweep_1.dat"; // just to store data file name, in future we need it
	final String file_2_name = "sweep_2.dat"; // just to store data file name, in future we need it
	ArrayList<Double> file_1_data = new ArrayList<Double>();
	ArrayList<Double> file_2_data = new ArrayList<Double>();
	public file_data(){
		// reads sweep 1 data, stores it
		file_1_data =  readfile(file_1_name);
		// reads sweep 1 data, stores it
		file_2_data = readfile(file_2_name);
		
			
		
	}
	ArrayList<Double> readfile(String filename){
		ArrayList<Double> file_data = new ArrayList<Double>();
		FileReader file_reader_obj = null;
		BufferedReader file_buffered_reader = null;
		try{
			// this will read all data from data files
			file_reader_obj = new FileReader(filename);
			file_buffered_reader = new BufferedReader(file_reader_obj);
			String file_line = file_buffered_reader.readLine();
			ArrayList<String> temp_memory = new ArrayList<String>();
			while(file_line != null){
				String[] line_items = file_line.split(" ");
				temp_memory = new ArrayList<String>(Arrays.asList(line_items));
				for(String adc_item : temp_memory) {
    					file_data.add(new Double(adc_item).doubleValue());
				}
				file_line = file_buffered_reader.readLine();
			}
		
		
		}catch(IOException e){
			e.printStackTrace(); // handles file reading errors
		}
		finally{
			try{
				
				// closes both objects after using them
				file_reader_obj.close();
				file_buffered_reader.close();
			}catch(IOException e){
				e.printStackTrace(); // handles file closing errors, if there were never opened
			}
			finally{}
			
		}
		
		return file_data;
		
	}
	
}
