sweep_1 = csvread('sweep_1_fourier_transform.csv');
sweep_2 = csvread('sweep_2_fourier_transform.csv');

figure
subplot(1,2,1)
stem(sweep_1(:,1),sweep_1(:,2));
title('Fourier Transform of Sweep 1');
xlabel('Frequency \omega(Hz)');
ylabel('|F(\omega)|');

subplot(1,2,2)
stem(sweep_2(:,1),sweep_2(:,2));
title('Fourier Transform of Sweep 2');
xlabel('Frequency \omega(Hz)');
ylabel('|F(\omega)|');
