import java.util.*;
class Fourier_Transform_Discrete{
	private int samples_size; // number of samples per sweep
	private final  double sampling_rate_frequency = 1*Math.pow(10,6); // FMCW 1 MHz sampling rate in time;
	final double T_sweep = 100*Math.pow(10,-6); // FMCW sweep time
	private double radar_bandwidth;
	ArrayList<Double> Radar_Data = new ArrayList<Double>();
	HashMap<Double,Double> Fourier_Transforms = new HashMap<Double,Double>();
	public Fourier_Transform_Discrete(ArrayList<Double> Time_domain_data, double fmcw_bandwidth){
		samples_size = Time_domain_data.size();
		radar_bandwidth = fmcw_bandwidth;
		Radar_Data = Time_domain_data;
		Discrete_time_Fourier_Transform();
	}
	
	void Discrete_time_Fourier_Transform(){
		double omega;
		double F_n_real,F_n_imaginary, f_k, sampling_time;
		for(int n = 0; n < samples_size; n++ ){
		        sampling_time = 1/sampling_rate_frequency;
			omega = n*(radar_bandwidth/sampling_rate_frequency)/(samples_size*sampling_time);
			F_n_real = 0;
			F_n_imaginary = 0;
			for(int k = 0; k < samples_size; k++){
				f_k = Radar_Data.get(k);
				/*here we use Euler's Formula for exponential of a constant e^(ix) = cos(x) + i*sin(x) 
				  but we break the equation into two parts, real(cosine) and imaginary(sine) 
				*/
				
				F_n_real = F_n_real + f_k*(Math.cos(omega*k));
				F_n_imaginary = F_n_imaginary + f_k*(-Math.sin(omega*k));
				
			}
			/*We store  the magnitude of the Radar  */
			Fourier_Transforms.put(omega, Math.sqrt(Math.pow(F_n_real,2) + Math.pow(F_n_imaginary,2)));
			
		}
	
	}
	
}
