import java.util.*;
import java.io.*;
public class program{
	// declaring constants now
	static final double c0 = 3*Math.pow(10,8); // speed of light constant
	static final double BW = 50*Math.pow(10,6); // FMCW bandwidth 50 MHz
	static final double T_sweep = 100*Math.pow(10,-6); // FMCW sweep time
 	public static void main(String[] args){
		file_data data = new file_data();  // create object to read from file, view the class file_data fro more details
		
		// this part analysis the data
		ArrayList<Double> sweep_1_adc = get_range_values(data.file_1_data);
		ArrayList<Double> sweep_2_adc = get_range_values(data.file_2_data);
		
		// computes frequencies using Faurier transform
		Fourier_Transform_Discrete Sweep_1_Fourier_Transform = new Fourier_Transform_Discrete(sweep_1_adc, BW);
		Fourier_Transform_Discrete Sweep_2_Fourier_Transform = new Fourier_Transform_Discrete(sweep_2_adc, BW);
		HashMap<Double,Double> Fourier_Transforms_Sweep_1 = Sweep_1_Fourier_Transform.Fourier_Transforms;
		HashMap<Double,Double> Fourier_Transforms_Sweep_2 = Sweep_2_Fourier_Transform.Fourier_Transforms;
		
		// store_values to file for processing later 
		write_data_file(new String("sweep_1_fourier_transform.csv"),Fourier_Transforms_Sweep_1);
		write_data_file(new String("sweep_2_fourier_transform.csv"),Fourier_Transforms_Sweep_2);
		
		
		// calculates the mean of each sweep to estimate the position of each 
		double beat_frequency_sweep_1 = find_frequency_maximum(Fourier_Transforms_Sweep_1);
		double beat_frequency_sweep_2 = find_frequency_maximum(Fourier_Transforms_Sweep_2);
		
		// Estimated range between sweeps
		double range_sweep_1 = range_function(beat_frequency_sweep_1);
		double range_sweep_2 = range_function(beat_frequency_sweep_2);
		double range_shift = range_function(Math.abs(beat_frequency_sweep_2 - beat_frequency_sweep_1));
		present_results(range_sweep_1, range_sweep_2, range_shift);
	}
	static void present_results(double sweep_1_range, double sweep_2_range, double shift_range){
		System.out.println("The program presents the following results :");
		System.out.println("	a. Estimated targer range for sweep 1 is "+ sweep_1_range + " meters");
		System.out.println("	b. Estimated targer range for sweep 2 is "+ sweep_2_range + " meters");
		System.out.println("	c. Estimated targer move between sweep 1 and sweep 2 is "+ shift_range + " meters");
	}
	
	
	// this method implement the given range radar equation
	static double range_function(double delt_f){
		double R = (c0*delt_f)/(2*(BW/T_sweep));
		return R;
	}
	// this method takes raw data ADC fro sweep convert it to range data 
	static ArrayList<Double> get_range_values(ArrayList<Double> sweep_reads){
		ArrayList<Double> range_values = new ArrayList<Double>();
		for(Double adc_read : sweep_reads){
			range_values.add(range_function(adc_read));
		}
		return range_values;
	}
	static void write_data_file(String file_name, HashMap<Double,Double> data_to_write){
		File file_obj = null;
		BufferedWriter file_bufferedwriter = null;
		try{
			file_obj = new File(file_name);
			file_bufferedwriter = new BufferedWriter(new FileWriter(file_obj));
			for(Map.Entry data : data_to_write.entrySet()){
				file_bufferedwriter.write(data.getKey()+ "," +data.getValue() + "\n");
			}
		
		}catch(IOException e){
			e.printStackTrace(); // handles file reading errors
		
		}
		finally{
			try{
			// closes both objects after using them
				file_bufferedwriter.close();
			}catch(IOException e){
				e.printStackTrace(); // handles file closing errors, if there were never opened
			}
			finally{}
		}
		
	}
	
	static double find_frequency_maximum(HashMap<Double,Double> fourier_transform_map){
		double high_power_frequency = 0.0;
		for(Map.Entry data : fourier_transform_map.entrySet()){
			if(fourier_transform_map.get(data.getKey()) > fourier_transform_map.get(high_power_frequency)){
				
				high_power_frequency = Double.valueOf(data.getKey().toString()).doubleValue();
				
			}
		}
		return high_power_frequency;
	
	}
}
