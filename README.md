######################## Read me file, please read ##############################

Author : Dinorego Mphogo

List of programs in repository: This is a Java 1.8* console program.

1. program.java and program.class contains main function for the task, does not take any arguments. It initilize file_data class to read from file. Also contain implementation of the program

2. file_data.java and file_data.class is a class file containing methods to read from data file below

3. sweep_1.dat and sweep_2.dat are data file containing radar data

4. Fourier_Transform_Discrete.java is a java code containing Fourier Transform

5. FourierTransform_plots.jpg is image containing Fourier Transofrm of the sleep data

6. sweep_1_fourier_transform.csv and sweep_2_fourier_transform.csv contains data of Fourier Transform

7. fourier_sweeps_plots.m is a octave plot 


How to run the program?
Ans : Compile Java program program.java or run the existing compiled version program.class
e.g "javac program.java" then follow with "java program"

Program output?
Ans : The output will output onto a standard terminal with plain text
e.g 

The program presents the following results :
	a. Estimated targer range for sweep 1 is 13350.0 meters
	b. Estimated targer range for sweep 2 is 2849.9999999999995 meters
	c. Estimated targer move between sweep 1 and sweep 2 is 10500.0 meters

